__author__ = "Danilo S. Carvalho <danilo@jaist.ac.jp>"

import copy

from saf import Document
from saf import Sentence
from saf import Token
from saf.importers.importer import Importer
from saf.constants import annotation


from epo_extraction.annotation import constants


class EPODocumentImporter(Importer):
    def __init__(self, word_tokenizers, pos_taggers=None):
        self.word_tokenizers = word_tokenizers
        self.pos_taggers = pos_taggers

    def import_document(self, patdoc, claims_only=False):
        doc = Document()
        doc.title = "-".join([patdoc["country"], patdoc["number"], patdoc["kind"]])
        doc.annotations["class_ipcr"] = [{"subclass": ipc[0], "subgroup": ipc[1]}
                                         for ipc in set([(ipc_info["subclass"], tuple(ipc_info["subgroup"]))
                                                         for ipc_info in patdoc["class_ipcr"]])]

        sentences = []

        if (not claims_only):
            for lang in patdoc["abstract"]:
                for sent in patdoc["abstract"][lang]:
                    sentences.append({"text": sent, "annotations": {constants.DOC_SECTION: "ABSTR", annotation.LANG: lang}})

            for lang in patdoc["description"]:
                for sent in patdoc["description"][lang]:
                    sentences.append({"text": sent, "annotations": {constants.DOC_SECTION: "DESCR", annotation.LANG: lang}})

        for lang in patdoc["claims"]:
            for claim in patdoc["claims"][lang]:
                claim_txt = " ".join(claim["text"])
                sentences.append({"text": claim_txt, "annotations": {constants.DOC_SECTION: "CLAIMS",
                                                                     constants.CLAIM_NUM: claim["number"],
                                                                     annotation.LANG: lang}})

        for sent in sentences:
            if (sent["text"] is not None and sent["text"] != ""):
                sentence = Sentence()
                sentence.annotations = copy.deepcopy(sent["annotations"])
                lang = sentence.annotations[annotation.LANG]

                if (self.pos_taggers is not None and lang in self.pos_taggers):
                    try:
                        for (token_raw, pos) in self.pos_taggers[lang](sent["text"]):
                            token = Token()
                            token.surface = token_raw
                            token.annotations[annotation.POS] = pos
                            sentence.tokens.append(token)
                    except:
                        print("Error tagging sentence: ", sent["text"])
                elif (lang in self.word_tokenizers):
                    try:
                        for token_raw in self.word_tokenizer[lang](sent["text"]):
                            token = Token()
                            token.surface = token_raw
                            token.annotations[annotation.DEPREL] = "0"
                            sentence.tokens.append(token)
                    except:
                        print("Error tokenizing sentence: ", sent["text"])
                else:
                    print("Language not available for tokenization:", lang)
                    exit(1)

                if (len(sentence.tokens) > 0):
                    doc.sentences.append(sentence)

        return doc


