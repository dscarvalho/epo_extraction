import json
import copy
from datetime import datetime
from typing import List, Dict


class PatDocMetaRef:
    def __init__(self):
        self.country: str = ""
        self.number: str = ""
        self.kind: str = ""
        self.date: str = ""


class PatDocParty:
    def __init__(self):
        self.name: str = ""
        self.country: str = ""


class PatentClaim:
    def __init__(self):
        self.number: int = 0
        self.text: List[str] = []
        self.refs: List[int] = []


class IPCInfo:
    def __init__(self):
        self.subclass: str = ""
        self.subgroup: List[str] = []
        self.version: str = ""
        self.classif_code: str = ""


class PatentDocument:
    def __init__(self):
        self.country: str = ""
        self.number: str = ""
        self.kind: str = ""
        self.date: str = ""
        self.application_ref: PatDocMetaRef = PatDocMetaRef()
        self.publication_ref: PatDocMetaRef = PatDocMetaRef()
        self.priority_claims: List[PatDocMetaRef] = []
        self.lang: str = ""
        self.class_ipcr: List[IPCInfo] = []
        self.title: Dict[str, List[str]] = {"EN": ""}
        self.applicants: List[PatDocParty] = []
        self.inventors: List[PatDocParty] = []
        self.agents: List[PatDocParty] = []
        self.designated_states: List[str] = []
        self.abstract: Dict[str, List[str]] = {"EN": []}
        self.description: Dict[str, List[str]] = {"EN": []}
        self.claims: Dict[str, List[PatentClaim]] = {"EN": []}
        self.claims_source: str = ""

    def to_dict(self):
        d_repr = copy.deepcopy(self.__dict__)
        d_repr["application_ref"] = self.application_ref.__dict__
        d_repr["publication_ref"] = self.publication_ref.__dict__
        d_repr["class_ipcr"] = []
        d_repr["priority_claims"] = []

        for ipc_info in self.class_ipcr:
            d_repr["class_ipcr"].append(ipc_info.__dict__)

        for pr_claim in self.priority_claims:
            d_repr["priority_claims"].append(pr_claim.__dict__)
        
        for field in ["applicants", "inventors", "agents"]:
            d_repr[field] = []
            for ent in getattr(self, field):
                d_repr[field].append(ent.__dict__)

        for lang in self.claims:
            d_repr["claims"][lang] = []
            for claim in self.claims[lang]:
                d_repr["claims"][lang].append(claim.__dict__)

        return d_repr

    def to_json(self):
        return json.dumps(self.to_dict())

    @staticmethod
    def from_json(json_doc):
        patdoc = PatentDocument()
        patdoc.country = json_doc["country"]
        patdoc.number = json_doc["number"]
        patdoc.kind = json_doc["kind"]
        patdoc.date = datetime(json_doc["date"])

        patdoc.application_ref.country = json_doc["application_ref"]["country"]
        patdoc.application_ref.number = json_doc["application_ref"]["number"]
        patdoc.application_ref.kind = json_doc["application_ref"]["kind"]
        patdoc.application_ref.date = json_doc["application_ref"]["date"]

        patdoc.publication_ref.country = json_doc["publication_ref"]["country"]
        patdoc.publication_ref.number = json_doc["publication_ref"]["number"]
        patdoc.publication_ref.kind = json_doc["publication_ref"]["kind"]
        patdoc.publication_ref.date = json_doc["publication_ref"]["date"]

        patdoc.priority_claims = json_doc["priority_claims"]

        patdoc.lang = json_doc["lang"]

        for ipcr in json_doc["class_ipcr"]:
            ipc_info = IPCInfo()
            ipc_info.subclass = ipcr["subclass"]
            ipc_info.subgroup = ipcr["subgroup"]
            ipc_info.classif_code = ipcr["classif_code"]
            ipc_info.version = ipcr["version"]
            patdoc.class_ipcr.append(ipc_info)

        for party in json_doc["applicants"]:
            applicant = PatDocParty()
            applicant.name = party["name"]
            applicant.country = party["country"]
            patdoc.applicants.append(applicant)

        for party in json_doc["inventors"]:
            inventor = PatDocParty()
            inventor.name = party["name"]
            inventor.country = party["country"]
            patdoc.inventors.append(inventor)

        for party in json_doc["agents"]:
            agent = PatDocParty()
            agent.name = party["name"]
            agent.country = party["country"]
            patdoc.agents.append(agent)

        patdoc.designated_states = json_doc["designated_states"]

        patdoc.title = json_doc["title"]
        patdoc.abstract = json_doc["abstract"]
        patdoc.description = json_doc["description"]

        for lang in json_doc["claims"]:
            if (lang not in patdoc.claims):
                patdoc.claims[lang] = list()

            for claim_info in json_doc["claims"][lang]:
                claim = PatentClaim()
                claim.number = claim_info["number"]
                claim.text = claim_info["text"]
                claim.refs = claim_info["refs"]
                patdoc.claims[lang].append(claim)

        patdoc.claims_source = json_doc["claims_source"]

        return patdoc

